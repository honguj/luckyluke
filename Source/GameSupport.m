//
//  GameSupport.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "GameSupport.h"
#import "ConstantManager.h"

@implementation GameSupport

+ (void)setupRandomPositionFor:(CCSprite*) sprite{
    int rndValue = 0 + arc4random() % (3 - 0);
    switch (rndValue) {
        case 0:
            sprite.positionInPoints = ccp([ConstantManager LEFT_POSITION_IN_POINTS], sprite.positionInPoints.y);
            break;
        case 1:
            sprite.positionInPoints = ccp([ConstantManager CENTER_POSITION_IN_POINTS], sprite.positionInPoints.y);
            break;
        case 2:
            sprite.positionInPoints = ccp([ConstantManager RIGHT_POSITION_IN_POINTS], sprite.positionInPoints.y);
            break;
        default:
            break;
    }
}

+ (void)loadParticleEffect:(NSString*)effectName onNode:(CCNode*)node{
    // load particle effect
    CCParticleSystem *explosion = (CCParticleSystem *)[CCBReader load:effectName];
    // make the particle effect clean itself up, once it is completed
    explosion.autoRemoveOnFinish = TRUE;
    // place the particle effect on the seals position
    explosion.position = node.position;
    // add the particle effect to the same node the seal is on
    [node.parent addChild:explosion];
}

//+ (void)playSoundEffect:(NSString*)soundName{
//    // access audio object
//    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
//    // play sound effect
//    [audio playEffect:soundName];
//}

+ (void)playSoundEffect:(NSString*)soundName loop:(BOOL)loop{
    // access audio object
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    // play sound effect
    [audio playEffect:soundName loop:loop];
}

+ (void)playMusicBackground{
    // Play random music background
    int rndValue = arc4random() % 3;
    NSString* songName = @"AdvMusic_AdventuringSong.mp3";
    switch (rndValue) {
        case 0:
            songName = @"AdvMusic_AdventuringSong.mp3";
            break;
        case 1:
            songName = @"AdvMusic_EpicEscape.mp3";
            break;
        case 2:
            songName = @"AdvMusic_TheRush2.mp3";
            break;
        default:
            break;
    }
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    // play sound effect
    [audio playBg:songName loop:true];
}

@end
