//
//  GameOverScene.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-04.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "GameOverScene.h"
#import "GameManager.h"

@implementation GameOverScene {
    CCLabelTTF* _distanceLabel;
    CCLabelTTF* _coinLabel;
}

- (void)didLoadFromCCB {
    [_distanceLabel setString:[NSString stringWithFormat:@"Distance: %f", [[GameManager sharedManager] distance]]];
    [_coinLabel setString:[NSString stringWithFormat:@"Coin: %i", [[GameManager sharedManager] coin]]];
}

- (void)replay{
    CCScene *gameplayScene = [CCBReader loadAsScene:@"GameScene"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
}

- (void)backToMenu{
    CCScene *mainMenuScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector] replaceScene:mainMenuScene];
}

@end
