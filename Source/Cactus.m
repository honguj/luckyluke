//
//  Cactus.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Cactus.h"
#import "GameManager.h"

@implementation Cactus

- (id)initObject{
    self = (Cactus*)[super initWithObjectName:@"Cactus"];
    self.life = [[GameManager sharedManager] getLevelProperty:CACTUS_LIFE];
    return self;
}

- (id)initObjectWithLife:(float)l{
    self = [self initObject];
    self.life = l;
    return self;
}

- (id)initAt:(CGPoint)position withLife:(float)l{
    self = (Cactus*)[super initWithObjectName:@"Cactus" at:position];
    self.life = l;
    return self;
}

@end
