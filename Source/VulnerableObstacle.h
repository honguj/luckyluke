//
//  VulnerableObstacle.h
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCSprite.h"
#import "Obstacle.h"

@interface VulnerableObstacle :Obstacle

@property (nonatomic) float life;
@property (nonatomic) bool hasCoin;

- (id)initWithObjectName:(NSString*)fileName;
- (id)initWithObjectName:(NSString*)fileName at:(CGPoint)position;

- (void)getDamaged:(float)damage;
- (void)getRemoved;
- (void)die;

// ANIMATIONS //
- (void)hurtAnimation;
- (void)defaultAnimation;
- (void)dieAnimation;

@end
