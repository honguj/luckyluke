//
//  Wagon.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Wagon.h"
#import "Bullet.h"
#import "ConstantManager.h"
#import "GameManager.h"

@implementation Wagon

- (id)initObject{
    self = (Wagon*)[super initWithObjectName:@"Wagon"];
    self.life = [[GameManager sharedManager] getLevelProperty:WAGON_LIFE];
    return self;
}

- (id)initObjectWithLife:(float)l{
    self = (Wagon*)[super initWithObjectName:@"Wagon"];
    self.life = l;
    return self;
}

- (id)initAt:(CGPoint)position withLife:(float)l{
    self = (Wagon*)[super initWithObjectName:@"Wagon" at:position];
    self.life = l;
    return self;
}

- (void)attackStrategy{
    [self schedule:@selector(shoot) interval:3];
}

- (void)shoot{
    if (self.physicsNode){
        CCActionCallBlock* addBullet = [CCActionCallBlock actionWithBlock:^{
            Bullet* bullet = [[Bullet alloc] initBulletAt:ccpSub(self.positionInPoints, ccp(0, self.contentSize.height/2+10)) withSpeed:-1.0f andRange:[ConstantManager BULLET_RANGE_DEFAULT] andDamage:[ConstantManager BULLET_DAMAGE_DEFAULT]];
            
            [self.physicsNode addChild:bullet];
        }];
        
        [self runAction:[CCActionSequence actions:
                         [CCActionCallFunc actionWithTarget:self selector:@selector(attackAnimation)],
                         [CCActionDelay actionWithDuration:0.5],
                         addBullet,
                         [CCActionCallFunc actionWithTarget:self selector:@selector(defaultAnimation)],
                         nil]];
    }
}

- (void)update:(CCTime)delta{
    [super update:delta];
    
    self.position = ccp(self.position.x, self.position.y + delta * 0.5 * [[GameManager sharedManager] getLevelProperty:SPEED]);
}



@end
