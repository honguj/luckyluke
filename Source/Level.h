//
//  Level.h
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-16.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Level : NSObject

typedef enum LevelPropertyType
{
    DISTANCE_BETWEEN_OBSTACLE,
    ENEMY_ATTACK_RATE,
    SPEED,
    ENEMY_SPAWN_RATE,
    CACTUS_LIFE,
    APACHE_LIFE,
    WAGON_LIFE,
    LevelPropertyCount
} LevelProperty;

@property (nonatomic) int tag;
@property (nonatomic) BOOL isLocked;
@property (nonatomic, retain) NSString* description;
@property (nonatomic, retain) NSString* backgroundName;
@property (nonatomic, retain) NSString* backgroundStoryName;
@property (nonatomic, retain) NSMutableArray* properties;

// To be implemented
@property (nonatomic, retain) NSMutableArray* enemies;

- (id)init;
- (id)initWithTag:(int)tagId description:(NSString*)desc backgroundImageName:(NSString*)bgImgName backgroundStoryImageName:(NSString*)bgStoryImgName;
- (float)getProperty:(LevelProperty) type;

@end
