//
//  GameManager.m
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-06.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "GameManager.h"
#import "ConstantManager.h"
#import "LevelParser.h"

@implementation GameManager

@synthesize coin;
@synthesize distance;
@synthesize heroLife;
@synthesize currentLevel;

+ (id)sharedManager {
    static GameManager *sharedGameManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedGameManager = [[self alloc] init];
    });
    return sharedGameManager;
}

- (void) initGame{
    coin = 0;
    distance = 0.0f;
    heroLife = [ConstantManager HERO_DEFAULT_LIFE];
}

- (void) accumulateCoin:(int)c andDistance:(float)dist{
    self.coin += c;
    self.distance += dist;
}

- (void) loadLevel:(int)levelId{
    LevelParser* lvlParser = [[LevelParser alloc] loadXMLLocal:[NSString stringWithFormat:@"Level%i",levelId]];
    self.currentLevel = lvlParser.level;
}

- (void) loadNextLevel{
    // Increase current Lvl
    int levelId = currentLevel.tag + 1;
    
    // Load new lvl from xml
    LevelParser* lvlParser = [[LevelParser alloc] loadXMLLocal:[NSString stringWithFormat:@"Level%i",levelId]];
    currentLevel = lvlParser.level;
    
    // Switch scene
    CCScene *levelStoryScene = [CCBReader loadAsScene:@"LevelStory/LevelStoryScene"];
    [[CCDirector sharedDirector] replaceScene:levelStoryScene];
}

- (float) getLevelProperty:(LevelProperty)propType{
    return [currentLevel getProperty:propType];
}

- (Obstacle*) popEnemyList{
    if (currentLevel.enemies.count == 0){
        return nil;
    }
    else {
        Obstacle* o = [currentLevel.enemies objectAtIndex:0];
        [currentLevel.enemies removeObjectAtIndex:0];
        return o;
    }
}

@end
