//
//  GameScene.h
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-04.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCScene.h"

typedef enum ObstacleTypes
{
    CACTUS_TYPE,
    ROCK_TYPE,
    WAGON_TYPE,
    APACHE_TYPE,
    numObstacleType
} ObstacleType;

@interface GameScene : CCScene<CCPhysicsCollisionDelegate>

- (void)attachGestureRecognizer;
- (void)detachGestureRecognizer;

@end
