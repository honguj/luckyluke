//
//  Level.m
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-16.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Level.h"
#import "ConstantManager.h"

@implementation Level

@synthesize tag;
@synthesize isLocked;
@synthesize description;
@synthesize backgroundName;
@synthesize backgroundStoryName;
@synthesize properties;
@synthesize enemies;

- (id)init{
    self = [super init];
    
    if (self){
        properties = [[NSMutableArray alloc] initWithCapacity:LevelPropertyCount];
        for (int i = 0; i < LevelPropertyCount; i++){
            [properties insertObject:[NSNumber numberWithFloat:1.0] atIndex:i];
        }
        enemies = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (id)initWithTag:(int)tagId description:(NSString*)desc backgroundImageName:(NSString*)bgImgName backgroundStoryImageName:(NSString*)bgStoryImgName {
    self = [super init];
    
    if (self){
        tag = tagId;
        description = desc;
        backgroundName = bgImgName;
        backgroundStoryName = bgStoryImgName;
        properties = [[NSMutableArray alloc] initWithCapacity:LevelPropertyCount];
        
        properties[DISTANCE_BETWEEN_OBSTACLE] = [NSNumber numberWithFloat:400];
        properties[CACTUS_LIFE] = [NSNumber numberWithFloat:2];
        properties[ENEMY_ATTACK_RATE] = [NSNumber numberWithFloat:1.0f];
        properties[SPEED] = [NSNumber numberWithFloat:1.0f];
        properties[ENEMY_SPAWN_RATE] = [NSNumber numberWithFloat:3.0f];
    }
    
    return self;
}

- (float)getProperty:(LevelProperty) type{
    return [properties[type] floatValue];
}

@end
