//
//  ConstantManager.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-04.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "ConstantManager.h"

@implementation ConstantManager

+ (int) LEFT_POSITION_IN_POINTS { return [CCDirector sharedDirector].viewSize.width * 0.15625; }

+ (int) CENTER_POSITION_IN_POINTS { return [CCDirector sharedDirector].viewSize.width * 0.5; }

+ (int) RIGHT_POSITION_IN_POINTS { return [CCDirector sharedDirector].viewSize.width * 0.84375; }

+ (int) MAX_LEVEL { return 3; }
// GAME SCENE
+ (float)SWITCH_LANE_SPEED { return 840.0f; }

// BULLET PROPERTIES
+ (float)BULLET_SPEED_DEFAULT { return 2; }
+ (float)BULLET_RANGE_DEFAULT { return 200; }
+ (float)BULLET_DAMAGE_DEFAULT { return 1; }

// HERO PROPERTIES
+ (float)HERO_DEFAULT_LIFE { return 15; }
+ (int) HERO_DEFAULT_BULLET_COUNT { return 6; }

// CATEGORY SCENE
+ (int)ROWS_PER_SCREEN { return 3; }
+ (int)COLUMNS_PER_SCREEN { return 3; }
+ (float)CATEGORY_HUD_HEIGHT { return 50; }
+ (float)CAT_BUTTON_FONT_SIZE { return 20.0f; }
+ (NSString*)CAT_BUTTON_COLOR_CODE { return @"#F79020"; }


@end
