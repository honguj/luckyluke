//
//  VulnerableDynamic.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "VulnerableDynamic.h"
#import "Bullet.h"

@implementation VulnerableDynamic

- (void)didLoadFromCCB {
    self.physicsBody.collisionType = @"vulnerableDynamic";
    self.physicsBody.type = CCPhysicsBodyTypeDynamic;
    self.physicsBody.elasticity = 0.0f;
    self.physicsBody.friction = 0.0f;
    self.physicsBody.affectedByGravity = false;
    self.physicsBody. allowsRotation = false;
 
    [self attackStrategy];
}

- (void)attackStrategy{
    
}

- (void)attackAnimation{
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Attack"];
}

@end
