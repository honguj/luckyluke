//
//  ConstantManager.h
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-04.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConstantManager : NSObject

+ (int) LEFT_POSITION_IN_POINTS;
+ (int) CENTER_POSITION_IN_POINTS;
+ (int) RIGHT_POSITION_IN_POINTS;
+ (int) MAX_LEVEL;

// GAME SCENE
+ (float)SWITCH_LANE_SPEED;

// Bullet properties
+ (float)BULLET_SPEED_DEFAULT;
+ (float)BULLET_RANGE_DEFAULT;
+ (float)BULLET_DAMAGE_DEFAULT;

// Hero properties
+ (float)HERO_DEFAULT_LIFE;
+ (int) HERO_DEFAULT_BULLET_COUNT;

// CATEGORY SCENE
+ (int)ROWS_PER_SCREEN;
+ (int)COLUMNS_PER_SCREEN;
+ (float)CATEGORY_HUD_HEIGHT;
+ (float)CAT_BUTTON_FONT_SIZE;
+ (NSString*)CAT_BUTTON_COLOR_CODE;

@end
