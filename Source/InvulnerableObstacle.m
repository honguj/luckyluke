//
//  InvulnerableObstacle.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "InvulnerableObstacle.h"
#import "GameSupport.h"

@implementation InvulnerableObstacle

- (id)initWithObjectName:(NSString*)fileName{
    self = (InvulnerableObstacle*)[CCBReader load:fileName];
    [GameSupport setupRandomPositionFor:self];
    return self;
}

- (id)initWithObjectName:(NSString*)fileName at:(CGPoint)position{
    self = (InvulnerableObstacle*)[CCBReader load:fileName];
    self.positionInPoints = position;
    [GameSupport setupRandomPositionFor:self];
    return self;
}

- (void)didLoadFromCCB {
    self.physicsBody.collisionType = @"invulnerableObstacle";
}

@end
