//
//  GameSupport.h
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameSupport : NSObject

+ (void)setupRandomPositionFor:(CCSprite*) sprite;
+ (void)loadParticleEffect:(NSString*)effectName onNode:(CCNode*)node;
//+ (void)playSoundEffect:(NSString*)soundName;
+ (void)playSoundEffect:(NSString*)soundName loop:(BOOL)loop;
+ (void)playMusicBackground;

@end
