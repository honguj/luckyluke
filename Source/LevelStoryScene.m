//
//  LevelStoryScene.m
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-16.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "LevelStoryScene.h"
#import "GameManager.h"

@implementation LevelStoryScene{
    CCSprite* _background;
    CCLabelTTF* _headerLabel;
    CCLabelTTF* _storyLabel;
}

- (void)didLoadFromCCB {
    Level* selectedLvl = [[GameManager sharedManager] currentLevel];
    
    [_background setSpriteFrame:[CCSpriteFrame frameWithImageNamed: selectedLvl.backgroundStoryName]];
    [_headerLabel setString:[NSString stringWithFormat:@"Mission %i", selectedLvl.tag]];
    [_storyLabel setString:selectedLvl.description];
    
    // Auto switch to game scene
    CCActionCallBlock* loadingGameScene = [CCActionCallBlock actionWithBlock:^{
        CCScene *gameScene = [CCBReader loadAsScene:@"GameScene"];
        [[CCDirector sharedDirector] replaceScene:gameScene];
    }];
    
    [self runAction:[CCActionSequence actions:
                     [CCActionDelay actionWithDuration:2.0f],
                     loadingGameScene,
                     nil]];
}

@end
