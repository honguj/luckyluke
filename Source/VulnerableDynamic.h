//
//  VulnerableDynamic.h
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCSprite.h"
#import "VulnerableObstacle.h"

@interface VulnerableDynamic : VulnerableObstacle

- (void)attackStrategy;

///////// ANIMATION ///////////
- (void)attackAnimation;

@end
