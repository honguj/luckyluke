//
//  Axe.m
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-07.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Axe.h"
#import "ConstantManager.h"
#import "GameManager.h"

@implementation Axe{
    float initY;
}

- (id)initBulletAt:(CGPoint)position{
    self = (Axe*)[CCBReader load:@"Axe"];
    self.positionInPoints = position;
    
    self.physicsBody.type = CCPhysicsBodyTypeDynamic;
    self.physicsBody.elasticity = 0.0f;
    self.physicsBody.friction = 0.0f;
    self.physicsBody.affectedByGravity = false;
    self.physicsBody. allowsRotation = false;
    
    
    initY = position.y;
    
    // Init default properties
    self.range = [ConstantManager BULLET_RANGE_DEFAULT];
    self.speed = [ConstantManager BULLET_SPEED_DEFAULT];
    self.damage = [ConstantManager BULLET_DAMAGE_DEFAULT];
    
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Default"];
    
    return self;
}

- (id) initBulletAt:(CGPoint)position withSpeed:(float)s andRange:(float)r andDamage:(float)d{
    self = [self initBulletAt:position];
    
    self.range = r;
    self.damage = d;
    self.speed = s;
    
    return self;
}

- (void)didLoadFromCCB {
    
    self.physicsBody.collisionType = @"bullet";
    self.physicsBody.collisionMask = @[@"vulnerableObstacle", @"invulnerableObstacle", @"hero"];
}

- (void)update:(CCTime)delta {
    self.position = ccp(self.position.x, self.position.y + delta * self.speed * [[GameManager sharedManager] getLevelProperty:SPEED]);
    
    if (abs(self.position.y - initY) >= self.range)
        [self.parent removeChild:self];
}

@end
