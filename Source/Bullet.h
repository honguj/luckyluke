//
//  Bullet.h
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-04.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCSprite.h"

@interface Bullet : CCSprite

@property (nonatomic) float range;
@property (nonatomic) float speed;
@property (nonatomic) float damage;


- (id)initBulletAt:(CGPoint)position;
- (id)initBulletAt:(CGPoint)position withSpeed:(float)speed andRange:(float)range andDamage:(float)damage;

@end
