//
//  Hero.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-04.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Hero.h"
#import "ConstantManager.h"
#import "GameSupport.h"
#import "GameManager.h"
#import "Bullet.h"

@implementation Hero {
    HeroPosition _currentPosition;
    bool _vulnerable;
    bool _isReloading;
    HeroPosition _destination;
}

@synthesize life;
@synthesize bulletCount;
@synthesize vx;
@synthesize isJumping;

- (void)didLoadFromCCB {
    life = [ConstantManager HERO_DEFAULT_LIFE];
    bulletCount = [ConstantManager HERO_DEFAULT_BULLET_COUNT];
    
    _currentPosition = CENTER_POSITION;
    _destination = CENTER_POSITION;
    self.physicsBody.collisionType = @"hero";
    _vulnerable = true;
    _isReloading = false;
    vx = 0;
    
    [self defaultAnimation];
}


// ACTIONS
- (void)getDamaged{
    if (_vulnerable){
        life -= 1;
    }
    [self runAction:[CCActionSequence actions:
                     [CCActionCallFunc actionWithTarget:self selector:@selector(hurtAnimation)],
                     [CCActionDelay actionWithDuration:1.0f],
                     [CCActionCallFunc actionWithTarget:self selector:@selector(defaultAnimation)]
                     , nil]];
}

- (void)shoot{
    if (bulletCount > 0){
        Bullet* bullet = [[Bullet alloc] initBulletAt:ccpAdd(self.positionInPoints, ccp(0, self.contentSize.height/2))];
        [self.physicsNode addChild:bullet];
        
        [self runAction:[CCActionSequence actions:
                         [CCActionCallFunc actionWithTarget:self selector:@selector(attackAnimation)],
                         [CCActionDelay actionWithDuration:0.2f],
                         [CCActionCallFunc actionWithTarget:self selector:@selector(defaultAnimation)]
                         , nil]];
        bulletCount--;
        [GameSupport playSoundEffect:@"gunshot.mp3" loop:false];
    }
}

- (void)reload{
    _isReloading = true;
    
    CCActionCallBlock* playSound = [CCActionCallBlock actionWithBlock:^{
        [GameSupport playSoundEffect:@"gunReload.mp3" loop:false];
    }];
    
    CCActionCallBlock* reloadAction = [CCActionCallBlock actionWithBlock:^{
        bulletCount = [ConstantManager HERO_DEFAULT_BULLET_COUNT];
        _isReloading = false;
        [GameSupport playSoundEffect:@"gunReload.mp3" loop:false];
    }];
    
    [self runAction:[CCActionSequence actions:
                     playSound,
                     [CCActionDelay actionWithDuration:2.0f],
                     reloadAction
                     , nil]];
}

- (void)move:(Direction) direction{
    switch (_currentPosition) {
        case LEFT_POSITION:
            if (direction == RIGHT) {
                vx = [ConstantManager SWITCH_LANE_SPEED];
                _destination = CENTER_POSITION;
            }
            break;
        case CENTER_POSITION:
            if (direction == RIGHT){
                vx = [ConstantManager SWITCH_LANE_SPEED];
                _destination = RIGHT_POSITION;
            }
            else if (direction == LEFT){
                vx = -[ConstantManager SWITCH_LANE_SPEED];
                _destination = LEFT_POSITION;
            }
            break;
        case RIGHT_POSITION:
            if (direction == LEFT){
                vx = -[ConstantManager SWITCH_LANE_SPEED];
                _destination = CENTER_POSITION;
            }
            break;
            
        default:
            break;
    }
}

- (void)update:(CCTime)delta {
    if (bulletCount == 0 && !_isReloading)
        [self reload];
    
    float scrollSpeed = [[GameManager sharedManager] getLevelProperty:SPEED];
    // TEST
    if (vx != 0 && _destination != _currentPosition && delta!=0 ){ // Swich lane
        float destinationX = [self heroPositionXInPoint:_destination];
        float f = fabsf((destinationX - self.positionInPoints.x)/(delta * vx));
        
        if (f >= 1){
            self.positionInPoints = ccp(self.positionInPoints.x + delta * vx, self.positionInPoints.y + delta * scrollSpeed);
        }
        else {
            self.positionInPoints = ccp(self.positionInPoints.x + delta * vx * f, self.positionInPoints.y + delta * scrollSpeed);
            _currentPosition = _destination;
            vx = 0;
        }
    } else {
        // Keep moving forward
        self.positionInPoints = ccp(self.positionInPoints.x, self.positionInPoints.y + delta * scrollSpeed);
    }
}

- (float)heroPositionXInPoint:(HeroPosition)positionX{
    switch (positionX) {
        case LEFT_POSITION:
            return [ConstantManager LEFT_POSITION_IN_POINTS];
            break;
        case CENTER_POSITION:
            return [ConstantManager CENTER_POSITION_IN_POINTS];
            break;
        case RIGHT_POSITION:
            return [ConstantManager RIGHT_POSITION_IN_POINTS];
            break;
        default:
            break;
    }
}

// ANIMATIONS
- (void)attackAnimation{
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Attack"];
}

- (void)defaultAnimation{
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Walk"];
    _vulnerable = true;
}

- (void)hurtAnimation{
    _vulnerable = false;
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Hurt"];
}

@end
