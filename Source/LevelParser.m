//
//  LevelParser.m
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-17.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "LevelParser.h"
#import "Obstacle.h"
#import "Apache.h"
#import "Cactus.h"
#import "Rock.h"
#import "Wagon.h"
#import "GameManager.h"

@implementation LevelParser

@synthesize level;

-(id) loadXMLByURL:(NSString *)urlString
{
    level = [[Level alloc] init];
	NSURL *url		= [NSURL URLWithString:urlString];
	NSData	*data   = [[NSData alloc] initWithContentsOfURL:url];
	parser			= [[NSXMLParser alloc] initWithData:data];
	parser.delegate = self;
	[parser parse];
	return self;
}

-(id) loadXMLLocal:(NSString *)fileName
{
	level = [[Level alloc] init];
    
    // Read data from XML
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"xml"];
    
    // Create NSData instance from xml in filePath
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];
    
	parser			= [[NSXMLParser alloc] initWithData:xmlData];
	parser.delegate = self;
	[parser parse];
	return self;
    
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if ([elementname isEqualToString:@"general"])
	{
        [level setTag:[[attributeDict objectForKey:@"tag"] intValue]];
        [level setDescription:[attributeDict objectForKey:@"description"]];
        [level setBackgroundName:[attributeDict objectForKey:@"background"]];
        [level setBackgroundStoryName:[attributeDict objectForKey:@"backgroundStory"]];
	} else if ([elementname isEqualToString:@"property"]){
        NSString* s = [attributeDict objectForKey:@"value"];
        NSNumber* n = [NSNumber numberWithFloat:[s floatValue]];
        [level.properties replaceObjectAtIndex:[[attributeDict objectForKey:@"index"] intValue] withObject:n];
    } else if ([elementname isEqualToString:@"enemy"]) {
        NSString* t = [attributeDict objectForKey:@"type"];
        Obstacle* obstacle = nil;
        float previousY = 430;
        if (level.enemies.count > 0){
            Obstacle* previousO = [level.enemies lastObject];
            previousY = previousO.positionInPoints.y;
        }
        
        if ([t isEqualToString:@"Rock"]){
            obstacle = [[Rock alloc] initAt:ccp(200, previousY + [[level.properties objectAtIndex:DISTANCE_BETWEEN_OBSTACLE] floatValue])];
        } else if ( [t isEqualToString:@"Cactus"]){
            obstacle = [[Cactus alloc] initAt:ccp(200, previousY + [[level.properties objectAtIndex:DISTANCE_BETWEEN_OBSTACLE] floatValue])
                                     withLife:[[level.properties objectAtIndex:CACTUS_LIFE] floatValue]];
        } else if ( [t isEqualToString:@"Apache"]){
            obstacle = [[Apache alloc] initAt:ccp(200, previousY + [[level.properties objectAtIndex:DISTANCE_BETWEEN_OBSTACLE] floatValue])
                                     withLife:[[level.properties objectAtIndex:APACHE_LIFE] floatValue]];
        } else if ( [t isEqualToString:@"Wagon"]){
            obstacle = [[Wagon alloc] initAt:ccp(200, previousY + [[level.properties objectAtIndex:DISTANCE_BETWEEN_OBSTACLE] floatValue])
                                     withLife:[[level.properties objectAtIndex:WAGON_LIFE] floatValue]];
        } else {
            return;
        }
        
        [level.enemies addObject:obstacle];
        NSLog(@"obstacle position: %f", obstacle.positionInPoints.y);
    }
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
}

- (void) dealloc
{
    
}

@end