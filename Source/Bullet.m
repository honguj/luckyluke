//
//  Bullet.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-04.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Bullet.h"
#import "ConstantManager.h"
#import "GameManager.h"

@implementation Bullet {
    float initY;
}

@synthesize range;
@synthesize speed;
@synthesize damage;

- (id)initBulletAt:(CGPoint)position{
    self = (Bullet*)[CCBReader load:@"Bullet"];
    self.positionInPoints = position;
    
    initY = position.y;
    
    // Init default properties
    range = [ConstantManager BULLET_RANGE_DEFAULT];
    speed = [ConstantManager BULLET_SPEED_DEFAULT];
    damage = [ConstantManager BULLET_DAMAGE_DEFAULT];
    
    return self;
}

- (id) initBulletAt:(CGPoint)position withSpeed:(float)s andRange:(float)r andDamage:(float)d{
    self = [self initBulletAt:position];
    
    self.range = r;
    self.damage = d;
    self.speed = s;
    
    return self;
}

- (void)didLoadFromCCB {
    
    self.physicsBody.collisionType = @"bullet";
    self.physicsBody.collisionMask = @[@"vulnerableObstacle", @"invulnerableObstacle", @"hero"];
}

- (void)update:(CCTime)delta {
    self.position = ccp(self.position.x, self.position.y + delta * speed *[[GameManager sharedManager] getLevelProperty:SPEED]);
    
    if (abs(self.position.y - initY) >= range)
        [self.parent removeChild:self];
}

@end
