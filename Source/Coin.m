//
//  Coin.m
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-06.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Coin.h"

@implementation Coin

- (id)initCoinAt:(CGPoint)position{
    self = (Coin*)[CCBReader load:@"Coin"];
    self.positionInPoints = position;
    
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Default"];
    
    return self;
}

- (void)didLoadFromCCB {
    self.physicsBody.collisionType = @"coin";
    self.physicsBody.collisionMask = @[@"hero"];
}

@end
