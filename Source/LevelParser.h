//
//  LevelParser.h
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-17.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Level.h"

@interface LevelParser : NSObject<NSXMLParserDelegate>{
    NSXMLParser		*parser;
}

@property (nonatomic, retain) Level* level;

-(id) loadXMLByURL:(NSString *)urlString;
-(id) loadXMLLocal:(NSString *)fileName;

@end
