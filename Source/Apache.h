//
//  Apache.h
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-07.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCSprite.h"
#import "VulnerableDynamic.h"

@interface Apache : VulnerableDynamic

- (id)initObject;
- (id)initObjectWithLife:(float)l;
- (id)initAt:(CGPoint)position withLife:(float)l;

@end
