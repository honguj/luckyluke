//
//  Coin.h
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-06.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCSprite.h"

@interface Coin : CCSprite

- (id)initCoinAt:(CGPoint)position;

@end
