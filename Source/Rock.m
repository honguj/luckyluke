//
//  Rock.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Rock.h"

@implementation Rock

- (id)initObject{
    self = [super initWithObjectName:@"Rock"];
    return self;
}

- (id)initAt:(CGPoint)position{
    self = (Rock*)[super initWithObjectName:@"Rock" at:position];
    return self;
}

@end
