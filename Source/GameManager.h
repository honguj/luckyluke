//
//  GameManager.h
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-06.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Level.h"
#import "Obstacle.h"

@interface GameManager : NSObject

@property (nonatomic) int coin;
@property (nonatomic) float distance;
@property (nonatomic) int heroLife;
@property (nonatomic, retain) Level* currentLevel;

+ (id)sharedManager;
- (void) initGame;
- (void) accumulateCoin:(int)coin andDistance:(float)dist;
- (void) loadLevel:(int)levelId;
- (void) loadNextLevel;
- (float) getLevelProperty:(LevelProperty)propType;
- (Obstacle*) popEnemyList;

@end
