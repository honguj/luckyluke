//
//  LevelScene.m
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-16.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "LevelScene.h"
#import "GameSupport.h"
#import "ConstantManager.h"
#import "GameManager.h"
#import "Level.h"

@implementation LevelScene {
    CCButton* _backButton;
    CCLabelTTF* _headerLabel;
}

// Temporary var
static int maxLevel = 3;

- (void)didLoadFromCCB {
    [self loadLevelGrid];
}

- (void) backToMenu{
    [GameSupport playSoundEffect:@"gunshot.mp3" loop:false];
    CCScene *mainScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector] replaceScene:mainScene];
}


- (void) loadLevelGrid{
    // Temporary variables
    float buttonWidth = 50.0f;
    float buttonHeight = 50.0f;
    int levels = 10;
    
    float numberRows = ceil(levels*1.0/[ConstantManager COLUMNS_PER_SCREEN]);
    NSLog(@"numberRows: %f", numberRows);
    float spacingX = (([CCDirector sharedDirector].viewSize.width/[ConstantManager COLUMNS_PER_SCREEN]) - buttonWidth)/2;
    float spacingY = ((([CCDirector sharedDirector].viewSize.height - [ConstantManager CATEGORY_HUD_HEIGHT])/ [ConstantManager ROWS_PER_SCREEN])- buttonHeight)/2;
    
    CCNode *cats = [CCNode node];
    float catsHeight = ceil(numberRows / [ConstantManager ROWS_PER_SCREEN]) * [CCDirector sharedDirector].viewSize.height;
    cats.contentSize = CGSizeMake([CCDirector sharedDirector].viewSize.width, catsHeight);
    
    CCSpriteFrame *imageOff = [CCSpriteFrame frameWithImageNamed:@"ccbButtonNormal.png"];
    CCSpriteFrame *imageOn = [CCSpriteFrame frameWithImageNamed:@"ccbButtonHighlighted.png"];

    int posX = spacingX + buttonWidth/2;
    int posY = cats.contentSize.height - spacingX - buttonHeight/2;
    
    int index = 0;
    for(int row=0; row < numberRows; row++){
        if (row % [ConstantManager ROWS_PER_SCREEN] == 0)
            posY -= [ConstantManager CATEGORY_HUD_HEIGHT];
        for (int col = 0; col < [ConstantManager COLUMNS_PER_SCREEN]; col++){
            index = row * [ConstantManager COLUMNS_PER_SCREEN] + col;
            if (index >= levels)
                break;
//            CategoryGame* cat = categories[index];
            
            // Category button
            CCButton* button = [CCButton buttonWithTitle:[NSString stringWithFormat:@"%i", index+1] spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
            button.label.fontSize = [ConstantManager CAT_BUTTON_FONT_SIZE];
//            button.label.fontColor = [CCColor colorWithRed:1 green:0 blue:0];
            button.position = ccp(posX, posY);
            button.contentSize = CGSizeMake(buttonWidth, buttonHeight);
            button.block = ^(id sender)
            {
                NSLog(@"Level %i selected", index + 1);
                if (index+1 <= maxLevel){
                    // Load level
                    [[GameManager sharedManager] loadLevel:(index+1)];
                    
                    [GameSupport playSoundEffect:@"gunshot.mp3" loop:false];
                    CCScene *levelStoryScene = [CCBReader loadAsScene:@"LevelStory/LevelStoryScene"];
                    [[CCDirector sharedDirector] replaceScene:levelStoryScene];
                }
            };
            posX += buttonWidth + spacingX * 2;
            [cats addChild:button z:0];
        }
        posX = spacingX + buttonWidth/2;
        posY -= (buttonHeight + spacingY * 2);
    }
    
    CCScrollView *catScroll = [[CCScrollView alloc] initWithContentNode:cats];
    [catScroll setAnchorPoint:ccp(0.0f, 0.0f)];
    [catScroll setPosition:ccp(0, 0)];
    [catScroll setPagingEnabled:NO];
    [catScroll setHorizontalScrollEnabled:NO];
    [self addChild:catScroll z:10 name:@"categories"];
}


@end
