//
//  Cactus.h
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCSprite.h"
#import "VulnerableObstacle.h"

@interface Cactus : VulnerableObstacle

- (id)initObject;
- (id)initObjectWithLife:(float)l;
- (id)initAt:(CGPoint)position withLife:(float)l;

@end
