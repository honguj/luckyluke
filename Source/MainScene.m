//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "MainScene.h"
#import "GameSupport.h"
#import "GameManager.h"

@implementation MainScene

- (void)play{
    [GameSupport playSoundEffect:@"gunshot.mp3" loop:false];
    
    [[GameManager sharedManager] initGame];
    [[GameManager sharedManager] loadLevel:1];
    
    CCScene *levelStoryScene = [CCBReader loadAsScene:@"LevelStory/LevelStoryScene"];
    [[CCDirector sharedDirector] replaceScene:levelStoryScene];
}

- (void)store{
    [GameSupport playSoundEffect:@"gunshot.mp3" loop:false];
}

- (void)help{
    [GameSupport playSoundEffect:@"gunshot.mp3" loop:false];
}

@end
