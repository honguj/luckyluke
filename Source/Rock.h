//
//  Rock.h
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCSprite.h"
#import "InvulnerableObstacle.h"

@interface Rock : InvulnerableObstacle

- (id)initObject;
- (id)initAt:(CGPoint)position;

@end
