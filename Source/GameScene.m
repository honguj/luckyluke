//
//  GameScene.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-04.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "GameScene.h"
#import "CCPhysics+ObjectiveChipmunk.h"
#import "Hero.h"
#import "ConstantManager.h"
#import "Obstacle.h"
#import "Bullet.h"
#import "Cactus.h"
#import "Rock.h"
#import "Wagon.h"
#import "GameOverScene.h"
#import "GameManager.h"
#import "GameSupport.h"
#import "Apache.h"

@implementation GameScene {
    Hero* _hero;
    CCPhysicsNode* _physicsNode;
    NSMutableArray *_obstacles;
    CCLabelTTF* _lifeStatus;
    CCLabelTTF* _coinStatus;
    CCLabelTTF* _bulletStatus;
    CCSprite* _ground1;
    CCSprite* _ground2;
    CCButton* _pauseButton;
    CCButton* _resumeButton;
    CCNode* _levelCompleteNode;
    CCLabelTTF* _coinLabel;
    NSArray *_grounds;
    int _coin;
    bool _levelComplete;
    
    UISwipeGestureRecognizer* swipeRightGestureRecognizer;
    UISwipeGestureRecognizer* swipeLeftGestureRecognizer;
    UITapGestureRecognizer *tapGestureRecognizer;
}

- (void)didLoadFromCCB {
    // tell this scene to accept touches
//    self.userInteractionEnabled = TRUE;
    
    // Setup gesture recognizer
    [self attachGestureRecognizer];
    
    // grounds scrolling
//    [_ground1 setSpriteFrame:[CCSpriteFrame frameWithImageNamed: @"GeneralAssets/DesertBg.png"]];
    _grounds = @[_ground1, _ground2];
    _levelCompleteNode.visible = false;
    
    _physicsNode.collisionDelegate = self;
//    _physicsNode.debugDraw = TRUE;
    
    _hero.zOrder = 100;
    _hero.life = [[GameManager sharedManager] heroLife];
    _coin = 0;
    _levelComplete = false;
    
    // Preload sound effects
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    [audio preloadEffect:@"gunshot.mp3"];
    [audio preloadEffect:@"gunReload.mp3"];
    [audio preloadEffect:@"explode.wav"];
    [audio preloadEffect:@"Coin.wav"];
    [audio preloadEffect:@"Apache.wav"];
    [audio preloadEffect:@"Horse-Gallop.mp3"];
    
    // Play music background
    [GameSupport playMusicBackground];
    [GameSupport playSoundEffect:@"Horse-Gallop.mp3" loop:true];
    
    // Spawn obstacles
    int numberToSpawn = ceilf([CCDirector sharedDirector].viewSize.height/[[GameManager sharedManager] getLevelProperty:DISTANCE_BETWEEN_OBSTACLE]);
    _obstacles = [NSMutableArray array];
//    numberToSpawn = [[[[GameManager sharedManager] currentLevel] enemies]count];
    NSLog(@"NumberToSpawn: %i", numberToSpawn);
    for (int i = 0; i < numberToSpawn+1; i++){
        [self spawnNewObstacle];
    }
}

- (void)attachGestureRecognizer{
    swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRightFrom:)];
    swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [[UIApplication sharedApplication].delegate.window addGestureRecognizer:swipeRightGestureRecognizer];
    
    swipeLeftGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeftFrom:)];
    swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [[UIApplication sharedApplication].delegate.window addGestureRecognizer:swipeLeftGestureRecognizer];
    
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    tapGestureRecognizer.delegate = self;
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [[UIApplication sharedApplication].delegate.window addGestureRecognizer:tapGestureRecognizer];
    
}

- (void)detachGestureRecognizer{
    [[UIApplication sharedApplication].delegate.window removeGestureRecognizer:swipeLeftGestureRecognizer];
    [[UIApplication sharedApplication].delegate.window removeGestureRecognizer:swipeRightGestureRecognizer];
    //    [[CCDirector sharedDirector].view removeGestureRecognizer:tapGestureRecognizer];
    [[UIApplication sharedApplication].delegate.window removeGestureRecognizer:tapGestureRecognizer];
}

- (void)pause{
    _pauseButton.visible = false;
    [[OALSimpleAudio sharedInstance] stopAllEffects];
    [[CCDirector sharedDirector] pause];
    _resumeButton.position = ccp([CCDirector sharedDirector].viewSize.width/2,
                                 [CCDirector sharedDirector].viewSize.height/2);
    
    [self detachGestureRecognizer];
}

- (void)resume{
    [GameSupport playSoundEffect:@"Horse-Gallop.mp3" loop:true];
    _pauseButton.visible = true;
    _resumeButton.position = ccp(0, -50);
    [self attachGestureRecognizer];
    [[CCDirector sharedDirector] resume];
}

- (void)update:(CCTime)delta {
    // Scroll physics world
    _physicsNode.position = ccp(_physicsNode.position.x, _physicsNode.position.y  - ([[GameManager sharedManager] getLevelProperty:SPEED] *delta));
    
    // Loop the grounds
    for (CCNode *ground in _grounds) {
        // get the world position of the ground
        CGPoint groundWorldPosition = [_physicsNode convertToWorldSpace:ground.position];
        // get the screen position of the ground
        CGPoint groundScreenPosition = [self convertToNodeSpace:groundWorldPosition];
        // if the left corner is one complete width off the screen, move it to the right
        if (groundScreenPosition.y <= (-1)*(ground.contentSize.height - 3)) {
            ground.position = ccp(ground.position.x, ground.position.y + 2 * (ground.contentSize.height-3));
        }
    }
    
    if (!_levelComplete){
        // Verify if all the enemies has been loaded and removed
        if (_obstacles.count == 0 && [[[[GameManager sharedManager] currentLevel] enemies] count] == 0){
            _levelComplete = true;
            if ([[[GameManager sharedManager] currentLevel] tag] == [ConstantManager MAX_LEVEL]){
                [self gameOver];
            }
            else{
                [self nextLevel];
            }
            return;
        }
        
        // Update life status
        if (_hero.life <= 0){
            _levelComplete = true;
            [self gameOver];
            NSLog(@"Distance: %f", _hero.positionInPoints.y);
            return;
        }
        else
            [_lifeStatus setString:[NSString stringWithFormat:@"Life: %i", _hero.life]];
        
        // Update coin status
        [_coinStatus setString:[NSString stringWithFormat:@"Coin: %i", _coin]];
        
        // Update bullets status
        if (_hero.bulletCount > 0){
            [_bulletStatus setString:[NSString stringWithFormat:@"x %i", _hero.bulletCount]];
        } else {
            [_bulletStatus setString:@"Reloading"];
        }
        
        // Remove all out-of-screen obstacles
        NSMutableArray *offScreenObstacles = nil;
        for (CCNode *obstacle in _obstacles) {
            CGPoint obstacleWorldPosition = [_physicsNode convertToWorldSpace:obstacle.position];
            CGPoint obstacleScreenPosition = [self convertToNodeSpace:obstacleWorldPosition];
            if (obstacleScreenPosition.y < 0) {
                if (!offScreenObstacles) {
                    offScreenObstacles = [NSMutableArray array];
                }
                [offScreenObstacles addObject:obstacle];
            }
        }
        for (CCNode *obstacleToRemove in offScreenObstacles) {
            [obstacleToRemove removeFromParent];
            [_obstacles removeObject:obstacleToRemove];
            // for each removed obstacle, add a new one
            [self spawnNewObstacle];
        }
    }
}

- (void)gameOver{
    [[GameManager sharedManager] accumulateCoin:_coin andDistance:(_hero.positionInPoints.y - 150)];
    
    GameOverScene *gameoverScene = (GameOverScene*)[CCBReader loadAsScene:@"GameOverScene"];
    [[CCDirector sharedDirector] replaceScene:gameoverScene];
    
    // Stop background music
    [[OALSimpleAudio sharedInstance] stopBg];
    [[OALSimpleAudio sharedInstance] stopAllEffects];	
    
    [self detachGestureRecognizer];
}

- (void)nextLevel{
    // Show Nextlevel panel
    [_coinLabel setString:[NSString stringWithFormat:@"Coin: +%i", _coin]];
    _levelCompleteNode.visible = true;
    
    // Desactivate gesture recognizer
    [self detachGestureRecognizer];
    
    // Save hero life
    [[GameManager sharedManager] setHeroLife:_hero.life];
    
    CCActionCallBlock* loadNextLvlAction = [CCActionCallBlock actionWithBlock:^{
        // Stop background music
        [[OALSimpleAudio sharedInstance] stopBg];
        [[OALSimpleAudio sharedInstance] stopAllEffects];
        
        // Accumulate coin and distance
        [[GameManager sharedManager] accumulateCoin:_coin andDistance:(_hero.positionInPoints.y - 150)];
        
        [[GameManager sharedManager] loadNextLevel];
    }];
    
    [self runAction:[CCActionSequence actions:
                     [CCActionDelay actionWithDuration:3.0f],
                     loadNextLvlAction,
                     nil]];
}

- (void)spawnNewObstacle {
    Obstacle* obstacle = [[GameManager sharedManager] popEnemyList];
    if (obstacle == nil){
        NSLog(@"NO MORE ENEMY");
        return;
    } else {
        NSLog(@"Spawn new %@ at %f - hero at %f", NSStringFromClass(obstacle.class), obstacle.positionInPoints.y, _hero.positionInPoints.y);
        
        [_physicsNode addChild:obstacle z:10];
        [_obstacles addObject:obstacle];
    }
    NSLog(@"After spawn new obstacle - obstacles: %i - enemies %lu", _obstacles.count, (unsigned long)[[[[GameManager sharedManager] currentLevel] enemies] count]);
}


////////////////// PHYSICS CONTACT //////////////////
-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair hero:(CCNode *)nodeA wildcard:(CCNode *)nodeB{
    [[_physicsNode space] addPostStepBlock:^{
        if ([nodeB.physicsBody.collisionType  isEqual: @"coin"]){
            [nodeB.parent removeChild:nodeB];
            _coin += 1;
            [GameSupport playSoundEffect:@"Coin.wav" loop:false];
        } else if ([nodeB.physicsBody.collisionType isEqual: @"invulnerableObstacle"] && _hero.isJumping){
            NSArray* tmp = _hero.physicsBody.collisionMask;
            CCActionCallBlock* ignorePhysics = [CCActionCallBlock actionWithBlock:^{
                _hero.physicsBody.collisionMask = @[];
            }];
            CCActionCallBlock* defaultPhysics = [CCActionCallBlock actionWithBlock:^{
                _hero.physicsBody.collisionMask = tmp;
                _hero.isJumping = false;
            }];
            [_hero runAction:[CCActionSequence actions:
                              ignorePhysics,
                              [CCActionDelay actionWithDuration:1],
                              defaultPhysics
                              , nil]];
        } else {
            Obstacle* obstacle = (Obstacle*)nodeB;
            [obstacle.parent removeChild:obstacle];
            
            [_hero getDamaged];
        }
    }key:nodeA];
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair bullet:(CCNode *)nodeA vulnerableObstacle:(CCNode *)nodeB{
    [[_physicsNode space] addPostStepBlock:^{
        Bullet* bullet = (Bullet*)nodeA;
        VulnerableObstacle* obstacle = (VulnerableObstacle*)nodeB;
        [obstacle getDamaged:bullet.damage];
        
        // Temporary code. To improve
        [nodeA.parent removeChild:nodeA];
    }key:nodeA];
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair bullet:(CCNode *)nodeA vulnerableDynamic:(CCNode *)nodeB{
    [[_physicsNode space] addPostStepBlock:^{
        Bullet* bullet = (Bullet*)nodeA;
        VulnerableObstacle* obstacle = (VulnerableObstacle*)nodeB;
        [obstacle getDamaged:bullet.damage];
        
        // Temporary code. To improve
        [nodeA.parent removeChild:nodeA];
        
    }key:nodeA];
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair bullet:(CCNode *)nodeA invulnerableObstacle:(CCNode *)nodeB{
    [[_physicsNode space] addPostStepBlock:^{
        [nodeA.parent removeChild:nodeA];
    }key:nodeA];
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair vulnerableDynamic:(CCNode *)nodeA vulnerableObstacle:(CCNode *)nodeB{
    [[_physicsNode space] addPostStepBlock:^{
        [nodeB.parent removeChild:nodeB];
        [GameSupport loadParticleEffect:@"Smoke" onNode:nodeA];
    }key:nodeA];
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair vulnerableDynamic:(CCNode *)nodeA invulnerableObstacle:(CCNode *)nodeB{
    [[_physicsNode space] addPostStepBlock:^{
        VulnerableDynamic* vd = (VulnerableDynamic*)nodeA;
        [vd die];
        [GameSupport loadParticleEffect:@"Explosion" onNode:nodeB];
        [GameSupport playSoundEffect:@"explode.wav" loop:false];
    }key:nodeA];
}

-(void)ccPhysicsCollisionPostSolve:(CCPhysicsCollisionPair *)pair vulnerableDynamic:(CCNode *)nodeA vulnerableDynamic:(CCNode *)nodeB{
    [[_physicsNode space] addPostStepBlock:^{
        [GameSupport loadParticleEffect:@"Explosion" onNode:nodeB];
        [GameSupport playSoundEffect:@"explode.wav" loop:false];
        
        [nodeA.parent removeChild:nodeA];
        [nodeB.parent removeChild:nodeB];
        
    }key:nodeA];
}

//////////////////// HANDLE USER ACTIONS ////////////////////
- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
    [_hero shoot];
}

- (void)handleSwipeRightFrom:(UIGestureRecognizer*)recognizer{
    [_hero move:RIGHT];
}

- (void)handleSwipeLeftFrom:(UIGestureRecognizer*)recognizer {
    [_hero move:LEFT];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    CGPoint pos = [touch locationInView: [CCDirector sharedDirector].view];
    pos = [[CCDirector sharedDirector] convertToGL:pos];
    
    if ([self buttonTouched:_pauseButton atPosition:pos]) {      //change it to your condition
        return NO;
    }
    return YES;
}

- (BOOL)buttonTouched:(CCButton*)button atPosition:(CGPoint)position{
    return (
            (position.x > (button.positionInPoints.x - button.contentSize.width/2))
            && (position.x < (button.positionInPoints.x + button.contentSize.width/2))
            && (position.y < (button.positionInPoints.y + button.contentSize.height/2))
            && (position.y > (button.positionInPoints.y - button.contentSize.height/2))
            );
}

@end
