//
//  Apache.m
//  LonelyCowboy
//
//  Created by Duc Nguyen on 2014-08-07.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Apache.h"
#import "Bullet.h"
#import "ConstantManager.h"
#import "GameSupport.h"
#import "Axe.h"
#import "GameManager.h"

@implementation Apache

- (id) initObject{
    self = (Apache*)[super initWithObjectName:@"Apache"];
    self.life = [[GameManager sharedManager] getLevelProperty:APACHE_LIFE];
    return self;
}

- (id)initObjectWithLife:(float)l{
    self = (Apache*)[super initWithObjectName:@"Apache"];
    self.life = l;
    return self;
}

- (id)initAt:(CGPoint)position withLife:(float)l{
    self = (Apache*)[super initWithObjectName:@"Apache" at:position];
    self.life = l;
    return self;
}

- (void)attackStrategy{
    [self schedule:@selector(shoot) interval:3];
}

- (void)shoot{
    if (self.physicsNode){
        CCActionCallBlock* addBullet = [CCActionCallBlock actionWithBlock:^{
            Axe* bullet = [[Axe alloc] initBulletAt:ccpSub(self.positionInPoints, ccp(0, self.contentSize.height/2+20)) withSpeed:-1.0f andRange:[ConstantManager BULLET_RANGE_DEFAULT] andDamage:[ConstantManager BULLET_DAMAGE_DEFAULT]];
            [self.physicsNode addChild:bullet];
        }];
        
        [self runAction:[CCActionSequence actions:
                         [CCActionCallFunc actionWithTarget:self selector:@selector(attackAnimation)],
                         [CCActionDelay actionWithDuration:0.5],
                         addBullet,
                         [CCActionCallFunc actionWithTarget:self selector:@selector(defaultAnimation)],
                         nil]];
    }
}

- (void)dieAnimation{
    [GameSupport playSoundEffect:@"Apache.wav" loop:false];
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Die"];
}

@end
