//
//  VulnerableObstacle.m
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-05.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "VulnerableObstacle.h"
#import "GameSupport.h"
#import "Coin.h"

@implementation VulnerableObstacle

@synthesize life;
@synthesize hasCoin;

- (id)initWithObjectName:(NSString*)fileName{
    self = (VulnerableObstacle*)[CCBReader load:fileName];
    
    [self defaultAnimation];
    
    // Random hasCoin
    int rndValue = arc4random() % (2);
    if (rndValue == 1)
        hasCoin = true;
    else
        hasCoin = false;
    
    // Init properties
    life = 2;
    
    // Random X
    [GameSupport setupRandomPositionFor:self];
    
    return self;
}

- (id)initWithObjectName:(NSString*)fileName at:(CGPoint)position{
    self = [self initWithObjectName:fileName];
    
    self.positionInPoints = position;
    [GameSupport setupRandomPositionFor:self];
    
    return self;
}

- (void)didLoadFromCCB {
    self.physicsBody.collisionType = @"vulnerableObstacle";
}

- (void)getDamaged:(float)damage{
    life -= damage;
    
    [self runAction:[CCActionSequence actions:
                     [CCActionCallFunc actionWithTarget:self selector:@selector(hurtAnimation)],
                     [CCActionDelay actionWithDuration:0.2f],
                     [CCActionCallFunc actionWithTarget:self selector:@selector(defaultAnimation)]
                     , nil]];
}

- (void)getRemoved{
    [self stopAllActions];
    [self.parent removeChild:self];
}

- (void)die{
    self.physicsBody.collisionMask = @[];
    CCActionCallBlock* produceCoinAction = [CCActionCallBlock actionWithBlock:^{
        if (hasCoin){
            Coin* coin = [[Coin alloc] initCoinAt:self.positionInPoints];
            [self.physicsNode addChild:coin];
        }
    }];
    [self runAction:[CCActionSequence actions:
                     [CCActionCallFunc actionWithTarget:self selector:@selector(dieAnimation)],
                     [CCActionDelay actionWithDuration:0.2f],
                     produceCoinAction,
                     [CCActionCallFunc actionWithTarget:self selector:@selector(getRemoved)]
                     , nil]];
}

- (void)update:(CCTime)delta {
    if (life <= 0) {
        [self die];
    }
}

- (void)hurtAnimation{
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Hurt"];
}

- (void)defaultAnimation{
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Walk"];
}

- (void)dieAnimation{
    CCAnimationManager* animationManager = self.animationManager;
    [animationManager runAnimationsForSequenceNamed:@"Die"];
}


@end
