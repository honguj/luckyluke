//
//  Hero.h
//  LuckyLuke
//
//  Created by Duc Nguyen on 2014-08-04.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCSprite.h"

typedef enum HeroPositionTypes
{
    LEFT_POSITION,
    CENTER_POSITION,
    RIGHT_POSITION
} HeroPosition;

typedef enum DirectionTypes
{
    LEFT,
    RIGHT
} Direction;

@interface Hero : CCSprite

@property (nonatomic) int life;
@property (nonatomic) int bulletCount;
@property (nonatomic) float vx;
@property (nonatomic) BOOL isJumping;

// ACTIONS
- (void)getDamaged;
- (void)shoot;
- (void)move:(Direction) direction;
- (void)reload;


// ANIMATIONS
- (void)attackAnimation;
- (void)defaultAnimation;
- (void)hurtAnimation;

@end
